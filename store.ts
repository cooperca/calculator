import create from "zustand";

type AppState = {
  postcode: string;
  income: number;
  term: Term;
  priceGrowth: number[];
  setPostcode: (name: string) => void;
  setIncome: (income: number) => void;
  setTerm: (term: Term) => void;
  setPriceGrowth: (priceGrowth: number[]) => void;
};

export enum Term {
  Three = 3,
  Five = 5,
  Seven = 7,
}

const useStore = create<AppState>((set) => ({
  postcode: "",
  income: 0,
  term: Term.Five,
  priceGrowth: [3.5],
  setPostcode: (postcode) => {
    set((state) => {
      return {
        ...state,
        postcode,
      };
    });
  },
  setIncome: (income) => {
    set((state) => {
      return {
        ...state,
        income,
      };
    });
  },
  setTerm: (term) => {
    set((state) => {
      return {
        ...state,
        term,
      };
    });
  },
  setPriceGrowth: (priceGrowth) => {
    set((state) => {
      return {
        ...state,
        priceGrowth,
      };
    });
  },
}));

export default useStore;
