import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html>
      <Head>
        <link
          href="https://fonts.googleapis.com/css2?family=Outfit&display=optional"
          rel="stylesheet"
        />
      </Head>
      <body className="bg-navy">
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
