import type { NextPage } from "next";
import Calculator from "../components/calculator";
import Results from "../components/Results";

const Home: NextPage = () => {
  return (
    <main className="bg-parchment bg-opacity-75 flex flex-row h-[100vh] max-w-[1200px] m-auto w-100vw">
      <div className="flex flex-1 flex-col justify-center m-20">
        <Calculator />
      </div>
      <div className="flex flex-1 flex-col justify-center m-20">
        <Results />
      </div>
    </main>
  );
};

export default Home;
