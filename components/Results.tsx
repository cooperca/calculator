import useStore from "../store";

const formatCurrencyValue = (value: number) => {
  return `£${Number(value).toLocaleString(undefined, {
    maximumFractionDigits: 2,
  })}`;
};

const Results = () => {
  const store = useStore();

  const maximumBudget = store.income * 6.25;
  const monthlyCost = (maximumBudget / 12) * 0.045;

  const growth = (1 + store.priceGrowth[0] / 100) ** store.term;
  const projectedValue = maximumBudget * growth;
  const totalConvertedRent = 0.25 * monthlyCost * (store.term * 12);
  // TODO: not sure if this should be projectedValue or the initial value of the property?
  const totalPayment = projectedValue - totalConvertedRent;

  return (
    <div>
      <h3 className="text-lg text-magenta">Product overview</h3>
      <h4>Maximum budget:</h4>
      <p className="text-azure">{formatCurrencyValue(maximumBudget)}</p>
      <h4>Monthly cost:</h4>
      <p className="text-azure">{formatCurrencyValue(monthlyCost)}</p>
      <h4>Product fee:</h4>
      <p className="text-azure">£2,999</p>
      <h3 className="text-lg text-magenta mt-4">
        Projection at end of year {store.term}
      </h3>
      <div>What is the projected value of the property?</div>
      <p className="text-azure">{formatCurrencyValue(projectedValue)}</p>
      <div>How much rent is converted?</div>
      <p className="text-azure">{formatCurrencyValue(totalConvertedRent)}</p>
      <div>How much do I have to pay?</div>
      <p className="text-azure">{formatCurrencyValue(totalPayment)}</p>
    </div>
  );
};

export default Results;
