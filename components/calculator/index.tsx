import { ChangeEvent, useMemo } from "react";
import useStore, { Term } from "../../store";
import Input from "./Input";
import PriceGrowthRange from "./PriceGrowthRange";
import TermSelector from "./TermSelector";
import Title from "./Title";

const Calculator = () => {
  const store = useStore();

  const handlePostcodeChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value.trim();
    if (value.length < 5) {
      store.setPostcode(value);
    }
  };

  const handleIncomeChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value.replace("£", "").replaceAll(",", "");

    const valueNumber = Number(value);

    // TODO: validate number is within range 25,000 and 1,000,000

    if (!isNaN(valueNumber)) {
      store.setIncome(Number(valueNumber));
    }
  };

  const handleTermSelectorClick = (term: Term) => {
    store.setTerm(term);
  };

  const handlePriceGrowthChange = (priceGrowth: number[]) => {
    store.setPriceGrowth(priceGrowth);
  };

  const parsedIncome = useMemo(
    () => `£${Number(store.income).toLocaleString()}`,
    [store.income]
  );

  return (
    <>
      <Title text="Where do you want to live?" />
      <Input value={store.postcode} onChange={handlePostcodeChange} />
      <Title className="mt-5" text="What is your annual income?" />
      <Input value={parsedIncome} onChange={handleIncomeChange} />
      <Title className="mt-5" text="How long do you want?" />
      <TermSelector
        selectedTerm={store.term}
        onClick={handleTermSelectorClick}
      />
      <Title className="mt-5" text="Assumed property price growth?" />
      <PriceGrowthRange
        priceGrowth={store.priceGrowth}
        onChange={handlePriceGrowthChange}
      />
    </>
  );
};

export default Calculator;
