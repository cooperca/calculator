import { ChangeEvent } from "react";

type InputProps = {
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
  value: string | number;
};

const Input = ({ onChange, value }: InputProps) => {
  return (
    <input
      className="bg-azure text-center text-slate-50 h-12"
      onChange={onChange}
      value={value}
    />
  );
};

export default Input;
