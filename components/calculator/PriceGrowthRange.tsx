import { Range } from "react-range";

type PriceGrowthRangeProps = {
  priceGrowth: number[];
  onChange: (priceGrowth: number[]) => void;
};

const PriceGrowthRange = ({ priceGrowth, onChange }: PriceGrowthRangeProps) => {
  return (
    <Range
      step={0.1}
      min={-1}
      max={7.5}
      values={priceGrowth}
      onChange={onChange}
      renderTrack={({ props, children }) => (
        <div {...props} className="w-full h-3 pr-2 my-4 bg-gray-400 rounded-md">
          {children}
        </div>
      )}
      renderThumb={({ props }) => {
        return (
          <div
            {...props}
            className="relative w-5 h-5 transform translate-x-10 bg-azure rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            <div className="absolute top-6 text-azure">{priceGrowth[0]}%</div>
          </div>
        );
      }}
    />
  );
};

export default PriceGrowthRange;
