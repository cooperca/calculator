type TitleProps = {
  className?: string;
  text: string;
};

const Title = ({ className, text }: TitleProps) => {
  return <h3 className={className}>{text}</h3>;
};

export default Title;
