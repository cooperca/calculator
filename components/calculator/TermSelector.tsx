import { Term } from "../../store";

type TermSelectorProps = {
  selectedTerm: number;
  onClick: (term: Term) => void;
};

const terms = [Term.Three, Term.Five, Term.Seven];

const TermSelector = ({ selectedTerm, onClick }: TermSelectorProps) => {
  return (
    <div className="flex">
      {terms.map((term, index) => {
        const isSelected = term === selectedTerm;

        const margin = index === 1 ? "mx-2" : "";
        const styles = isSelected
          ? "bg-azure text-slate-50"
          : "bg-gray-400 border-gray-700 text-gray-700";

        return (
          <div
            key={term}
            className={`${margin} ${styles} flex flex-col align-middle justify-center  border-[1px] cursor-pointer flex-1 h-12  text-center`}
            onClick={() => onClick(term)}
          >
            <p>{`${term} years`}</p>
          </div>
        );
      })}
    </div>
  );
};

export default TermSelector;
