const colors = require("tailwindcss/colors");

module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    fontFamily: {
      sans: ["Outfit"],
    },
    colors: {
      transparent: "transparent",
      current: "currentColor",
      navy: "#273043",
      honey: "#F39237",
      magenta: "#BF1363",
      azure: "#0E79B2",
      parchment: "#FBFFF1",
      gray: colors.gray,
      slate: colors.slate,
    },
    extend: {},
  },
  plugins: [],
};
